library ieee;
use ieee.std_logic_1164.all;

entity tb_l1 is
end tb_l1;

architecture tb_behavior of tb_l1 IS 

	-- Constants for generics
	constant c_test_addr_width : integer := 12;
	constant c_test_data_width : integer := 32;
	constant c_test_memory_depth : integer := 4; --��� ������������ ����� ������������ ������ ������� �������

   component l1
		generic (
			g_addr_width : integer;
			g_data_width : integer;
			g_memory_depth : integer
		);
		port( 
			i_clk : in std_logic;
			i_clr : in std_logic;
			i_wr : in std_logic;
			i_rd : in std_logic;
			i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_miss : out std_logic;
			o_data : out std_logic_vector (g_data_width - 1 downto 0)
		);
	end component;

	-- Inputs
	signal w_clr : std_logic := '0';
	signal w_clk : std_logic := '0';
	signal w_wr : std_logic := '0';
	signal w_rd : std_logic := '0';
	signal w_i_data : std_logic_vector(c_test_data_width - 1 downto 0) := (others => '0');
	signal w_addr : std_logic_vector(c_test_addr_width - 1 downto 0) := (others => '0');

	-- Outputs
	signal w_o_data : std_logic_vector(31 downto 0);
	signal w_miss : std_logic;

	-- Clock period definitions
	constant clk_p : time := 10 ns;

begin
	-- Instantiate the Unit Under Test (UUT)
	uut: l1
		generic map(
			g_addr_width => c_test_addr_width,
			g_data_width => c_test_data_width,
			g_memory_depth => c_test_memory_depth
		)
		port map(
			i_clr => w_clr,
			i_clk => w_clk,
			i_wr => w_wr,
			i_rd => w_rd,
			i_addr => w_addr,
			i_data => w_i_data,
			o_miss => w_miss,
			o_data => w_o_data
		);

	-- Clock
	w_clk <= not w_clk after clk_p / 2;
 
	-- Stimulus process
	stim_proc: process
	begin
		wait for clk_p * 10;
		w_clr <= '1';
		wait for clk_p;
		w_clr <= '0';
		w_wr <= '1';
		w_i_data <= x"AABBCCDD";
		w_addr <= x"ABC";
		wait for clk_p;
		w_wr <= '0';
		wait for clk_p * 10;
		w_wr <= '1';
		w_i_data <= x"00110011";
		w_addr <= x"001";
		wait for clk_p;
		w_wr <= '0';
		wait for clk_p * 10;
		w_rd <= '1';
		w_addr <= x"ABC";
		wait for clk_p;
		w_rd <= '0';
		wait for clk_p * 10;
		w_rd <= '1';
		w_addr <= x"000";
		wait for clk_p;
		w_rd <= '0';
		wait for clk_p * 10;
		w_wr <= '1';
		w_i_data <= x"AAAAAAAA";
		w_addr <= x"000";
		wait for clk_p;
		w_wr <= '0';
		wait for clk_p * 10;
		w_wr <= '1';
		w_i_data <= x"01234567";
		w_addr <= x"ABC";
		wait for clk_p;
		w_wr <= '0';
		wait for clk_p * 10;
		w_rd <= '1';
		w_addr <= x"000";
		wait for clk_p;
		w_rd <= '0';
		wait for clk_p * 10;
		w_rd <= '1';
		w_addr <= x"ABC";
		wait for clk_p;
		w_rd <= '0';
		wait for clk_p * 10;
		w_wr <= '1';
		w_addr <= x"111";
		w_i_data <= x"76543210";
		wait for clk_p;
		w_wr <= '0';
		wait for clk_p * 10;
		w_rd <= '1';
		w_addr <= x"000";
		wait for clk_p;
		w_rd <= '0';
		wait for clk_p * 10;
		w_wr <= '1';
		w_addr <= x"FFF";
		w_i_data <= x"77777777";
		wait for clk_p;
		w_wr <= '0';
		wait for clk_p * 10;
		wait;
	end process;
end;
