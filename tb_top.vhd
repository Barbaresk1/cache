library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
--USE ieee.numeric_std.ALL;
--LIBRARY UNISIM;
--USE UNISIM.Vcomponents.ALL;

entity tb_top is
end tb_top;

architecture tb_behavioral of tb_top is

	component top
		port(
			sys_w_ack : out std_logic;
			sys_r_ack : out std_logic;
			clk : in std_logic;
			clr : in std_logic;
			clk_2 : in std_logic;
			clk_op : in std_logic;
			sys_data_out : out std_logic_vector (7 downto 0);
			sys_wr : in std_logic;
			sys_rd : in std_logic;
			sys_data_in : in std_logic_vector (7 downto 0);
			sys_addr : in std_logic_vector (13 downto 0)
		);
	end component;

	signal w_sys_w_ack : std_logic;
	signal w_sys_r_ack : std_logic;
	signal w_clk : std_logic := '0';
	signal w_clr : std_logic := '0';
	signal w_clk_2 : std_logic := '0';
	signal w_clk_op : std_logic := '0';
	signal w_sys_data_out : std_logic_vector (7 downto 0);
	signal w_sys_wr : std_logic := '0';
	signal w_sys_rd : std_logic := '0';
	signal w_sys_data_in : std_logic_vector (7 downto 0) := (others => '0');
	signal w_sys_addr : std_logic_vector (15 downto 0) := (others => '0');

	-- Clock period definitions
	constant clk_p : time := 10 ns;
	constant clk_p_2 : time := 30 ns;
	constant clk_p_op : time := 110 ns;

begin

	uut: top 
		port map(
			sys_w_ack => w_sys_w_ack, 
			sys_r_ack => w_sys_r_ack, 
			clk => w_clk, 
			clr => w_clr, 
			clk_2 => w_clk_2, 
			clk_op => w_clk_op, 
			sys_data_out => w_sys_data_out, 
			sys_wr => w_sys_wr, 
			sys_rd => w_sys_rd, 
			sys_data_in => w_sys_data_in, 
			sys_addr => w_sys_addr(13 downto 0)
		);

	-- Clock
	w_clk <= not w_clk after clk_p / 2;
	w_clk_2 <= not w_clk_2 after clk_p_2 / 2;
	w_clk_op <= not w_clk_op after clk_p_op / 2;

	-- Stimulus process
	stim_proc: process
	begin
		w_clr <= '1';
		wait for clk_p * 100;
		w_clr <= '0';
		wait for clk_p * 100;
		w_sys_wr <= '1';
		w_sys_data_in <= x"AB";
		w_sys_addr <= x"1234";
		wait for clk_p;
		w_sys_wr <= '0';
		wait until w_sys_w_ack = '1' and rising_edge(w_clk);
		w_sys_rd <= '1';
		wait for clk_p;
		w_sys_rd <= '0';
		wait until w_sys_r_ack = '1' and rising_edge(w_clk);
		w_sys_rd <= '1';
		w_sys_addr <= x"0000";
		wait for clk_p;
		w_sys_rd <= '0';
		wait until w_sys_r_ack = '1' and rising_edge(w_clk);
		w_sys_rd <= '1';
		w_sys_addr <= x"0000";
		wait for clk_p;
		w_sys_rd <= '0';
		wait until w_sys_r_ack = '1' and rising_edge(w_clk);
		for i in 0 to 31 loop
			w_sys_data_in <= conv_std_logic_vector(i + 4, w_sys_data_in'length);
			w_sys_addr <= conv_std_logic_vector(i + 4, w_sys_addr'length);
			w_sys_wr <= '1';
			wait for clk_p;
			w_sys_wr <= '0';
			wait until w_sys_w_ack = '1' and rising_edge(w_clk);
		end loop;
		for i in 0 to 5 loop
			w_sys_data_in <= conv_std_logic_vector(i + 4, w_sys_data_in'length);
			w_sys_addr <= conv_std_logic_vector(7 * i + 100, w_sys_addr'length);
			w_sys_wr <= '1';
			wait for clk_p;
			w_sys_wr <= '0';
			wait until w_sys_w_ack = '1' and rising_edge(w_clk);
		end loop;
		for i in 0 to 10 loop
			w_sys_addr <= conv_std_logic_vector(i + 4, w_sys_addr'length);
			w_sys_rd <= '1';
			wait for clk_p;
			w_sys_rd <= '0';
			wait until w_sys_r_ack = '1' and rising_edge(w_clk);
		end loop;
		w_sys_addr <= conv_std_logic_vector(300, w_sys_addr'length);
		w_sys_rd <= '1';
		wait for clk_p;
		w_sys_rd <= '0';
		wait until w_sys_r_ack = '1' and rising_edge(w_clk);
		w_sys_addr <= conv_std_logic_vector(128, w_sys_addr'length);
		w_sys_rd <= '1';
		wait for clk_p;
		w_sys_rd <= '0';
		wait until w_sys_r_ack = '1' and rising_edge(w_clk);
		w_sys_addr <= conv_std_logic_vector(25 * 4, w_sys_addr'length);
		wait for clk_p;
		w_sys_rd <= '1';
		wait for clk_p;
		w_sys_rd <= '0';
		wait until w_sys_r_ack = '1' and rising_edge(w_clk);

		wait;
	end process;

end;