library ieee;
use ieee.std_logic_1164.all;

entity tb_ram is
end tb_ram;

architecture tb_behavior of tb_ram is

	-- Constants for generics
	constant c_test_addr_width : integer := 14;
	constant c_test_data_width : integer := 8;
	constant c_test_bl : integer := 4;

	component ram
		generic (
			g_addr_width : integer;
			g_data_width : integer;
			g_bl : integer
		);
		port( 
			i_clk : in std_logic;
			i_clr : in std_logic;
			i_cas : in std_logic;
			i_ras: in std_logic;
			i_wr : in std_logic;
			i_cs: in std_logic;
			i_addr : in std_logic_vector (g_addr_width / 2 - 1 downto 0);
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_data : out std_logic_vector (g_data_width - 1 downto 0)
		);
	end component;

	-- Inputs
	signal w_clr : std_logic := '0';
	signal w_clk : std_logic := '0';
	signal w_cas : std_logic := '0';
	signal w_ras : std_logic := '0';
	signal w_wr : std_logic := '0';
	signal w_cs : std_logic := '0';
	signal w_i_data : std_logic_vector(c_test_data_width - 1 downto 0) := (others => '0');
	signal w_addr : std_logic_vector(c_test_addr_width / 2 downto 0) := (others => '0');

	-- Outputs
	signal w_o_data : std_logic_vector(c_test_data_width - 1 downto 0);

	-- Clock period definitions
	constant clk_p : time := 10 ns;
 
begin

	-- Instantiate the Unit Under Test (UUT)
	uut: ram
		generic map(
			g_addr_width => c_test_addr_width,
			g_data_width => c_test_data_width,
			g_bl => c_test_bl
		)
		port map(
			i_clr => w_clr,
			i_clk => w_clk,
			i_cas => w_cas,
			i_ras => w_ras,
			i_wr => w_wr,
			i_cs => w_cs,
			i_addr => w_addr(c_test_addr_width / 2 - 1 downto 0),
			i_data => w_i_data,
			o_data => w_o_data
		);

	-- Clock
	w_clk <= not w_clk after clk_p / 2;
 
	-- Stimulus process
	stim_proc: process
	begin	
		-- hold reset state for 100 ns.
		wait for clk_p * 10;	
		w_clr <= '1';
		wait for clk_p;
		w_clr <= '0';
		wait for clk_p;
		w_clr <= '0';
		
		-- w_wr(0004) = DDCCBBAA
		w_addr <= x"00";
		w_ras <= '1';
		wait for clk_p;
		w_ras <= '0';
		w_addr <= x"05";
		w_cas <= '1';
		wait for clk_p;
		w_cas <= '0';
		w_wr <= '1';
		w_i_data <= x"DD";
		wait for clk_p;
		w_wr <= '0';
		w_i_data <= x"CC";
		wait for clk_p;
		w_i_data <= x"BB";
		wait for clk_p;
		w_i_data <= x"AA";
		wait for clk_p * 10;

		--rd(0006)
		w_addr <= x"00";
		w_ras <= '1';
		wait for clk_p;
		w_ras <= '0';
		w_addr <= x"06";
		w_cas <= '1';
		wait for clk_p;
		w_cas <= '0';
		w_cs <= '1';
		wait for clk_p;
		w_cs <= '0';
		wait for clk_p * 10;

		--rd(3FFF)
		w_addr <= x"FF";
		w_ras <= '1';
		wait for clk_p;
		w_ras <= '0';
		w_addr <= x"7F";
		w_cas <= '1';
		wait for clk_p;
		w_cas <= '0';
		w_cs <= '1';
		wait for clk_p;
		w_cs <= '0';
		wait for clk_p * 10;

		-- w_wr(3FFC) = 00AABB00
		w_addr <= x"FF";
		w_ras <= '1';
		wait for clk_p;
		w_ras <= '0';
		w_addr <= x"FE";
		w_cas <= '1';
		wait for clk_p;
		w_cas <= '0';
		w_wr <= '1';
		w_i_data <= x"00";
		wait for clk_p;	
		w_wr <= '0';
		w_i_data <= x"AA";
		wait for clk_p;
		w_i_data <= x"BB";
		wait for clk_p;
		w_i_data <= x"00";
		wait for clk_p * 10;

		--rd(3FFC)
		w_addr <= x"FF";
		w_ras <= '1';
		wait for clk_p;
		w_ras <= '0';
		w_addr <= x"FC";
		w_cas <= '1';
		wait for clk_p;
		w_cas <= '0';
		w_cs <= '1';
		wait for clk_p;		
		w_cs <= '0';
		wait for clk_p * 3;

      --rd(0006)
		w_addr <= x"00";
		w_ras <= '1';
		wait for clk_p;
		w_ras <= '0';
		w_addr <= x"06";
		w_cas <= '1';
		wait for clk_p;
		w_cas <= '0';
		w_cs <= '1';
		wait for clk_p;
		w_cs <= '0';
		wait for clk_p * 10;
		wait;
	end process;
	
end;