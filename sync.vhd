library ieee;
use ieee.std_logic_1164.all;

entity sync is
	generic(
		g_data_width : integer
	);
	port(
		i_clr : in std_logic;
		i_clk_1 : in std_logic;
		i_clk_2 : in std_logic;
		i_data : in std_logic_vector(g_data_width - 1 downto 0);
		o_data : out std_logic_vector(g_data_width - 1 downto 0);
		o_correct : out std_logic;
		o_is_changed : out std_logic
	);
end sync;

architecture sync_arch of sync is
	signal r_is_defined : boolean := false; --������ ����� true, ����� ��������� ������� ������
	signal r_is_first : boolean;
	signal r_is_changed : boolean;
	signal r_old_changed : boolean;
	signal r_counter : integer := 0; --������� ��� ����������� ����������� ������
	signal r_data_1 : std_logic_vector(g_data_width - 1 downto 0);
	signal r_data_2 : std_logic_vector(g_data_width - 1 downto 0);
	signal w_clk : std_logic;
begin

	w_clk <= i_clk_1 when r_is_first else i_clk_2; 

	--������� ��� ����������� ������� �������
	process (i_clk_1, i_clk_2, i_clr) begin
		if i_clr = '1' then
			r_is_defined <= false;
			r_counter <= 0;
		elsif not r_is_defined then
			if rising_edge(i_clk_1) then
				r_counter <= r_counter + 1;
			end if;
			if rising_edge(i_clk_2) then
				r_counter <= r_counter - 1;
			end if;
			if r_counter = 2 then
				r_is_defined <= true;
				r_is_first <= true;
			elsif r_counter = -2 then
				r_is_defined <= true;
				r_is_first <= false;
			end if;		
		end if;
	end process;
	
	--������� ��� ���������� �������, ���������� �� ������� �������
	process (w_clk, i_clr) begin
		if i_clr = '1' then
			r_data_2 <= (others => '0');
			r_data_1 <= (others => '0');
		elsif rising_edge(w_clk) and r_is_defined then
			r_data_2 <= r_data_1;
			r_data_1 <= i_data;
		end if;
	end process;
	
	--������� ��������� ������� � ������ ��������, �������� �� ������ �������
	process (i_clk_2, i_clr) begin
		if i_clr = '1' then
			r_is_changed <= false;
			o_data <= (others => '0');
			o_correct <= '0';
		elsif rising_edge(i_clk_2) then
			if r_data_1 = r_data_2 and i_data = r_data_1 then
				o_data <= r_data_2;
				o_correct <= '1';
				r_is_changed <= not r_is_changed;
			else
				o_data <= (others => '0');
				o_correct <= '0';
			end if;
		end if;
	end process;
	
	--������� ��� ����������� ����� ��������� ������ ��������������
	process (w_clk, i_clr) begin
		if i_clr = '1' then
			o_is_changed <= '0';
		elsif rising_edge(w_clk) then
			if r_is_changed /= r_old_changed then
				r_old_changed <= r_is_changed;
				o_is_changed <= '1';
			else
				o_is_changed <= '0';
			end if;
		end if;
	end process;

end sync_arch;

