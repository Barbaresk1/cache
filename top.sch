<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="sys_w_ack" />
        <signal name="sys_r_ack" />
        <signal name="w_l1_wr" />
        <signal name="w_l1_rd" />
        <signal name="w_l2_wr" />
        <signal name="w_l2_rd" />
        <signal name="w_ram_wr" />
        <signal name="w_ram_rd" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="clk" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="clr" />
        <signal name="XLXN_20" />
        <signal name="clk_2" />
        <signal name="clk_op" />
        <signal name="sys_data_out(7:0)" />
        <signal name="XLXN_24(31:0)" />
        <signal name="XLXN_25(31:0)" />
        <signal name="w_mem_data(31:0)" />
        <signal name="w_ram_addr(13:0)" />
        <signal name="w_l_addr(11:0)" />
        <signal name="XLXN_29(11:0)" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34(13:0)" />
        <signal name="XLXN_35(31:0)" />
        <signal name="sys_wr" />
        <signal name="sys_rd" />
        <signal name="w_l1_miss" />
        <signal name="w_l1_data(31:0)" />
        <signal name="w_l2_miss" />
        <signal name="w_l2_ready" />
        <signal name="w_l2_changed" />
        <signal name="w_ram_data(31:0)" />
        <signal name="w_ram_changed" />
        <signal name="w_ram_ready" />
        <signal name="w_l2_data(31:0)" />
        <signal name="sys_data_in(7:0)" />
        <signal name="sys_addr(13:0)" />
        <port polarity="Output" name="sys_w_ack" />
        <port polarity="Output" name="sys_r_ack" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="clr" />
        <port polarity="Input" name="clk_2" />
        <port polarity="Input" name="clk_op" />
        <port polarity="Output" name="sys_data_out(7:0)" />
        <port polarity="Input" name="sys_wr" />
        <port polarity="Input" name="sys_rd" />
        <port polarity="Input" name="sys_data_in(7:0)" />
        <port polarity="Input" name="sys_addr(13:0)" />
        <blockdef name="l1">
            <timestamp>2017-12-23T9:44:53</timestamp>
            <rect width="256" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="l2_sync">
            <timestamp>2017-12-23T9:44:59</timestamp>
            <rect width="288" x="64" y="-448" height="448" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="416" y1="-416" y2="-416" x1="352" />
            <line x2="416" y1="-288" y2="-288" x1="352" />
            <line x2="416" y1="-160" y2="-160" x1="352" />
            <rect width="64" x="352" y="-44" height="24" />
            <line x2="416" y1="-32" y2="-32" x1="352" />
        </blockdef>
        <blockdef name="ram_sync">
            <timestamp>2017-12-23T9:45:5</timestamp>
            <rect width="288" x="64" y="-448" height="448" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="416" y1="-416" y2="-416" x1="352" />
            <line x2="416" y1="-224" y2="-224" x1="352" />
            <rect width="64" x="352" y="-44" height="24" />
            <line x2="416" y1="-32" y2="-32" x1="352" />
        </blockdef>
        <blockdef name="cd_fsm">
            <timestamp>2017-12-23T9:47:52</timestamp>
            <rect width="64" x="480" y="20" height="24" />
            <line x2="544" y1="32" y2="32" x1="480" />
            <line x2="0" y1="-928" y2="-928" x1="64" />
            <line x2="0" y1="-864" y2="-864" x1="64" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="544" y1="-928" y2="-928" x1="480" />
            <line x2="544" y1="-848" y2="-848" x1="480" />
            <line x2="544" y1="-768" y2="-768" x1="480" />
            <line x2="544" y1="-688" y2="-688" x1="480" />
            <line x2="544" y1="-608" y2="-608" x1="480" />
            <line x2="544" y1="-528" y2="-528" x1="480" />
            <line x2="544" y1="-448" y2="-448" x1="480" />
            <line x2="544" y1="-368" y2="-368" x1="480" />
            <rect width="64" x="480" y="-300" height="24" />
            <line x2="544" y1="-288" y2="-288" x1="480" />
            <rect width="64" x="480" y="-220" height="24" />
            <line x2="544" y1="-208" y2="-208" x1="480" />
            <rect width="64" x="480" y="-60" height="24" />
            <line x2="544" y1="-48" y2="-48" x1="480" />
            <rect width="416" x="64" y="-960" height="1024" />
        </blockdef>
        <block symbolname="l1" name="l1_inst">
            <blockpin signalname="clk" name="i_clk" />
            <blockpin signalname="clr" name="i_clr" />
            <blockpin signalname="w_l1_wr" name="i_wr" />
            <blockpin signalname="w_l1_rd" name="i_rd" />
            <blockpin signalname="w_l_addr(11:0)" name="i_addr(11:0)" />
            <blockpin signalname="w_mem_data(31:0)" name="i_data(31:0)" />
            <blockpin signalname="w_l1_miss" name="o_miss" />
            <blockpin signalname="w_l1_data(31:0)" name="o_data(31:0)" />
        </block>
        <block symbolname="l2_sync" name="l2_inst">
            <blockpin signalname="clk" name="i_clk" />
            <blockpin signalname="clk_2" name="i_clk_2" />
            <blockpin signalname="clr" name="i_clr" />
            <blockpin signalname="w_l2_wr" name="i_wr" />
            <blockpin signalname="w_l2_rd" name="i_rd" />
            <blockpin signalname="w_l_addr(11:0)" name="i_addr(11:0)" />
            <blockpin signalname="w_mem_data(31:0)" name="i_data(31:0)" />
            <blockpin signalname="w_l2_miss" name="o_miss" />
            <blockpin signalname="w_l2_ready" name="o_ready" />
            <blockpin signalname="w_l2_changed" name="o_is_changed" />
            <blockpin signalname="w_l2_data(31:0)" name="o_data(31:0)" />
        </block>
        <block symbolname="ram_sync" name="ram_inst">
            <blockpin signalname="clk" name="i_clk" />
            <blockpin signalname="clk_op" name="i_clk_op" />
            <blockpin signalname="clr" name="i_clr" />
            <blockpin signalname="w_ram_wr" name="i_wr" />
            <blockpin signalname="w_ram_rd" name="i_rd" />
            <blockpin signalname="w_ram_addr(13:0)" name="i_addr(13:0)" />
            <blockpin signalname="w_mem_data(31:0)" name="i_data(31:0)" />
            <blockpin signalname="w_ram_ready" name="o_ready" />
            <blockpin signalname="w_ram_changed" name="o_is_changed" />
            <blockpin signalname="w_ram_data(31:0)" name="o_data(31:0)" />
        </block>
        <block symbolname="cd_fsm" name="cd_inst">
            <blockpin signalname="clk" name="i_clk" />
            <blockpin signalname="clr" name="i_clr" />
            <blockpin signalname="sys_wr" name="i_wr" />
            <blockpin signalname="sys_rd" name="i_rd" />
            <blockpin signalname="w_l1_miss" name="i_l1_miss" />
            <blockpin signalname="w_l2_ready" name="i_l2_ready" />
            <blockpin signalname="w_l2_miss" name="i_l2_miss" />
            <blockpin signalname="w_l2_changed" name="i_l2_changed" />
            <blockpin signalname="w_ram_ready" name="i_ram_ready" />
            <blockpin signalname="w_ram_changed" name="i_ram_changed" />
            <blockpin signalname="sys_addr(13:0)" name="i_addr(13:0)" />
            <blockpin signalname="sys_data_in(7:0)" name="i_data(7:0)" />
            <blockpin signalname="w_l1_data(31:0)" name="i_l1_data(31:0)" />
            <blockpin signalname="w_l2_data(31:0)" name="i_l2_data(31:0)" />
            <blockpin signalname="w_ram_data(31:0)" name="i_ram_data(31:0)" />
            <blockpin signalname="sys_w_ack" name="o_w_ack" />
            <blockpin signalname="sys_r_ack" name="o_r_ack" />
            <blockpin signalname="w_l1_wr" name="o_l1_wr" />
            <blockpin signalname="w_l1_rd" name="o_l1_rd" />
            <blockpin signalname="w_l2_wr" name="o_l2_wr" />
            <blockpin signalname="w_l2_rd" name="o_l2_rd" />
            <blockpin signalname="w_ram_wr" name="o_ram_wr" />
            <blockpin signalname="w_ram_rd" name="o_ram_rd" />
            <blockpin signalname="sys_data_out(7:0)" name="o_data(7:0)" />
            <blockpin signalname="w_mem_data(31:0)" name="o_mem_data(31:0)" />
            <blockpin signalname="w_ram_addr(13:0)" name="o_ram_addr(13:0)" />
            <blockpin signalname="w_l_addr(11:0)" name="o_l_addr(11:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="688" y="1744" name="cd_inst" orien="R0">
        </instance>
        <branch name="sys_w_ack">
            <wire x2="1248" y1="816" y2="816" x1="1232" />
        </branch>
        <branch name="sys_r_ack">
            <wire x2="1248" y1="896" y2="896" x1="1232" />
        </branch>
        <iomarker fontsize="28" x="1248" y="816" name="sys_w_ack" orien="R0" />
        <iomarker fontsize="28" x="1248" y="896" name="sys_r_ack" orien="R0" />
        <branch name="w_l1_wr">
            <wire x2="1456" y1="976" y2="976" x1="1232" />
            <wire x2="1456" y1="640" y2="976" x1="1456" />
            <wire x2="2064" y1="640" y2="640" x1="1456" />
        </branch>
        <branch name="w_l1_rd">
            <wire x2="1520" y1="1056" y2="1056" x1="1232" />
            <wire x2="1520" y1="704" y2="1056" x1="1520" />
            <wire x2="2064" y1="704" y2="704" x1="1520" />
        </branch>
        <branch name="w_l2_wr">
            <wire x2="1248" y1="1136" y2="1136" x1="1232" />
            <wire x2="1248" y1="1136" y2="1168" x1="1248" />
            <wire x2="2064" y1="1168" y2="1168" x1="1248" />
        </branch>
        <branch name="w_l2_rd">
            <wire x2="1248" y1="1216" y2="1216" x1="1232" />
            <wire x2="1248" y1="1216" y2="1232" x1="1248" />
            <wire x2="2064" y1="1232" y2="1232" x1="1248" />
        </branch>
        <instance x="2064" y="864" name="l1_inst" orien="R0">
        </instance>
        <branch name="w_ram_wr">
            <wire x2="1648" y1="1296" y2="1296" x1="1232" />
            <wire x2="1648" y1="1296" y2="1680" x1="1648" />
            <wire x2="2064" y1="1680" y2="1680" x1="1648" />
        </branch>
        <branch name="w_ram_rd">
            <wire x2="1632" y1="1376" y2="1376" x1="1232" />
            <wire x2="1632" y1="1376" y2="1744" x1="1632" />
            <wire x2="2064" y1="1744" y2="1744" x1="1632" />
        </branch>
        <branch name="clk">
            <wire x2="640" y1="816" y2="816" x1="624" />
            <wire x2="688" y1="816" y2="816" x1="640" />
            <wire x2="640" y1="512" y2="816" x1="640" />
            <wire x2="1872" y1="512" y2="512" x1="640" />
            <wire x2="2064" y1="512" y2="512" x1="1872" />
            <wire x2="1872" y1="512" y2="976" x1="1872" />
            <wire x2="2064" y1="976" y2="976" x1="1872" />
            <wire x2="1872" y1="976" y2="1488" x1="1872" />
            <wire x2="2064" y1="1488" y2="1488" x1="1872" />
        </branch>
        <branch name="clr">
            <wire x2="656" y1="880" y2="880" x1="624" />
            <wire x2="688" y1="880" y2="880" x1="656" />
            <wire x2="656" y1="576" y2="880" x1="656" />
            <wire x2="1904" y1="576" y2="576" x1="656" />
            <wire x2="2064" y1="576" y2="576" x1="1904" />
            <wire x2="1904" y1="576" y2="1104" x1="1904" />
            <wire x2="2064" y1="1104" y2="1104" x1="1904" />
            <wire x2="1904" y1="1104" y2="1616" x1="1904" />
            <wire x2="2064" y1="1616" y2="1616" x1="1904" />
        </branch>
        <branch name="clk_2">
            <wire x2="2064" y1="1040" y2="1040" x1="2048" />
        </branch>
        <branch name="clk_op">
            <wire x2="2064" y1="1552" y2="1552" x1="2048" />
        </branch>
        <iomarker fontsize="28" x="2048" y="1040" name="clk_2" orien="R180" />
        <branch name="sys_data_out(7:0)">
            <wire x2="1248" y1="1456" y2="1456" x1="1232" />
        </branch>
        <iomarker fontsize="28" x="1248" y="1456" name="sys_data_out(7:0)" orien="R0" />
        <branch name="w_mem_data(31:0)">
            <wire x2="1680" y1="1536" y2="1536" x1="1232" />
            <wire x2="1680" y1="1536" y2="1872" x1="1680" />
            <wire x2="2064" y1="1872" y2="1872" x1="1680" />
            <wire x2="2064" y1="832" y2="832" x1="1680" />
            <wire x2="1680" y1="832" y2="1360" x1="1680" />
            <wire x2="1680" y1="1360" y2="1536" x1="1680" />
            <wire x2="2064" y1="1360" y2="1360" x1="1680" />
        </branch>
        <branch name="w_ram_addr(13:0)">
            <wire x2="1392" y1="1696" y2="1696" x1="1232" />
            <wire x2="1392" y1="1696" y2="1808" x1="1392" />
            <wire x2="2064" y1="1808" y2="1808" x1="1392" />
        </branch>
        <branch name="w_l_addr(11:0)">
            <wire x2="1696" y1="1776" y2="1776" x1="1232" />
            <wire x2="2064" y1="768" y2="768" x1="1696" />
            <wire x2="1696" y1="768" y2="1296" x1="1696" />
            <wire x2="1696" y1="1296" y2="1776" x1="1696" />
            <wire x2="2064" y1="1296" y2="1296" x1="1696" />
        </branch>
        <instance x="2064" y="1392" name="l2_inst" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2048" y="1552" name="clk_op" orien="R180" />
        <instance x="2064" y="1904" name="ram_inst" orien="R0">
        </instance>
        <branch name="sys_wr">
            <wire x2="688" y1="944" y2="944" x1="624" />
        </branch>
        <branch name="sys_rd">
            <wire x2="688" y1="1008" y2="1008" x1="624" />
        </branch>
        <iomarker fontsize="28" x="624" y="944" name="sys_wr" orien="R180" />
        <iomarker fontsize="28" x="624" y="1008" name="sys_rd" orien="R180" />
        <iomarker fontsize="28" x="624" y="816" name="clk" orien="R180" />
        <iomarker fontsize="28" x="624" y="880" name="clr" orien="R180" />
        <branch name="w_l1_miss">
            <wire x2="480" y1="400" y2="1072" x1="480" />
            <wire x2="688" y1="1072" y2="1072" x1="480" />
            <wire x2="2560" y1="400" y2="400" x1="480" />
            <wire x2="2560" y1="400" y2="512" x1="2560" />
            <wire x2="2560" y1="512" y2="512" x1="2448" />
        </branch>
        <branch name="w_l1_data(31:0)">
            <wire x2="2592" y1="368" y2="368" x1="448" />
            <wire x2="2592" y1="368" y2="832" x1="2592" />
            <wire x2="448" y1="368" y2="1584" x1="448" />
            <wire x2="688" y1="1584" y2="1584" x1="448" />
            <wire x2="2592" y1="832" y2="832" x1="2448" />
        </branch>
        <branch name="w_l2_miss">
            <wire x2="416" y1="336" y2="1200" x1="416" />
            <wire x2="688" y1="1200" y2="1200" x1="416" />
            <wire x2="2624" y1="336" y2="336" x1="416" />
            <wire x2="2624" y1="336" y2="976" x1="2624" />
            <wire x2="2624" y1="976" y2="976" x1="2480" />
        </branch>
        <branch name="w_l2_ready">
            <wire x2="2656" y1="304" y2="304" x1="384" />
            <wire x2="2656" y1="304" y2="1104" x1="2656" />
            <wire x2="384" y1="304" y2="1136" x1="384" />
            <wire x2="688" y1="1136" y2="1136" x1="384" />
            <wire x2="2656" y1="1104" y2="1104" x1="2480" />
        </branch>
        <branch name="w_l2_changed">
            <wire x2="352" y1="272" y2="1264" x1="352" />
            <wire x2="688" y1="1264" y2="1264" x1="352" />
            <wire x2="2688" y1="272" y2="272" x1="352" />
            <wire x2="2688" y1="272" y2="1232" x1="2688" />
            <wire x2="2688" y1="1232" y2="1232" x1="2480" />
        </branch>
        <branch name="w_ram_data(31:0)">
            <wire x2="688" y1="1712" y2="1712" x1="656" />
            <wire x2="656" y1="1712" y2="1936" x1="656" />
            <wire x2="2512" y1="1936" y2="1936" x1="656" />
            <wire x2="2512" y1="1872" y2="1872" x1="2480" />
            <wire x2="2512" y1="1872" y2="1936" x1="2512" />
        </branch>
        <branch name="w_ram_changed">
            <wire x2="688" y1="1392" y2="1392" x1="624" />
            <wire x2="624" y1="1392" y2="1968" x1="624" />
            <wire x2="2544" y1="1968" y2="1968" x1="624" />
            <wire x2="2544" y1="1680" y2="1680" x1="2480" />
            <wire x2="2544" y1="1680" y2="1968" x1="2544" />
        </branch>
        <branch name="w_ram_ready">
            <wire x2="592" y1="1328" y2="2000" x1="592" />
            <wire x2="2576" y1="2000" y2="2000" x1="592" />
            <wire x2="688" y1="1328" y2="1328" x1="592" />
            <wire x2="2576" y1="1488" y2="1488" x1="2480" />
            <wire x2="2576" y1="1488" y2="2000" x1="2576" />
        </branch>
        <branch name="w_l2_data(31:0)">
            <wire x2="560" y1="1648" y2="2032" x1="560" />
            <wire x2="2608" y1="2032" y2="2032" x1="560" />
            <wire x2="688" y1="1648" y2="1648" x1="560" />
            <wire x2="2608" y1="1360" y2="1360" x1="2480" />
            <wire x2="2608" y1="1360" y2="2032" x1="2608" />
        </branch>
        <branch name="sys_data_in(7:0)">
            <wire x2="688" y1="1520" y2="1520" x1="416" />
        </branch>
        <iomarker fontsize="28" x="416" y="1520" name="sys_data_in(7:0)" orien="R180" />
        <branch name="sys_addr(13:0)">
            <wire x2="688" y1="1456" y2="1456" x1="416" />
        </branch>
        <iomarker fontsize="28" x="416" y="1456" name="sys_addr(13:0)" orien="R180" />
    </sheet>
</drawing>