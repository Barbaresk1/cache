library ieee;
use ieee.std_logic_1164.all;
use work.functions.all;

entity tb_cd_fsm is
end tb_cd_fsm;
 
architecture behavior of tb_cd_fsm is 

	-- Constants for generics
	constant c_test_addr_width : integer := 14;
	constant c_test_data_width : integer := 8;
	constant c_test_bl : integer := 4;

	-- Component Declaration for the Unit Under Test (UUT)

	component cd_fsm
		generic(
			g_data_width : integer;
			g_addr_width : integer;
			g_bl : integer
		);
		port( 
			i_clk : in std_logic;
			i_clr : in std_logic;
			i_wr : in std_logic;
			i_rd : in std_logic;
			i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_w_ack : out std_logic;
			o_r_ack : out std_logic;
			o_data : out std_logic_vector (g_data_width - 1 downto 0);
			o_mem_data : out std_logic_vector (g_data_width * g_bl - 1 downto 0);
			o_l_addr : out std_logic_vector (g_addr_width - log2(g_bl) - 1 downto 0);	
			i_l1_miss : in std_logic;
			i_l1_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
			o_l1_wr : out std_logic;
			o_l1_rd : out std_logic;
			i_l2_ready : in std_logic;
			i_l2_miss : in std_logic;
			i_l2_changed : in std_logic;
			i_l2_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
			o_l2_wr : out std_logic;
			o_l2_rd : out std_logic;
			i_ram_ready : in std_logic;
			i_ram_changed : in std_logic;
			i_ram_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
			o_ram_wr : out std_logic;
			o_ram_rd : out std_logic;
			o_ram_addr : out std_logic_vector (g_addr_width - 1 downto 0)
		);
	end component;

	--Inputs
	signal w_clk : std_logic := '0';
	signal w_clr : std_logic := '0';
	signal w_wr : std_logic := '0';
	signal w_rd : std_logic := '0';
	signal w_addr : std_logic_vector(2 + c_test_addr_width - 1 downto 0) := (others => '0');
	signal w_i_data : std_logic_vector(c_test_data_width - 1 downto 0) := (others => '0');
	signal w_l1_miss : std_logic := '0';
	signal w_l1_data : std_logic_vector(c_test_data_width * c_test_bl - 1 downto 0) := (others => '0');
	signal w_l2_ready : std_logic := '0';
	signal w_l2_miss : std_logic := '0';
	signal w_l2_changed : std_logic := '0';
	signal w_l2_data : std_logic_vector(c_test_data_width * c_test_bl - 1 downto 0) := (others => '0');
	signal w_ram_ready : std_logic := '0';
	signal w_ram_changed : std_logic := '0';
	signal w_ram_data : std_logic_vector(c_test_data_width * c_test_bl - 1 downto 0) := (others => '0');

 	--Outputs
	signal w_w_ack : std_logic;
	signal w_r_ack : std_logic;
	signal w_o_data : std_logic_vector(c_test_data_width - 1 downto 0);
	signal w_mem_data : std_logic_vector(c_test_data_width * c_test_bl - 1 downto 0);
	signal w_l_addr : std_logic_vector(c_test_addr_width - log2(c_test_bl) - 1 downto 0);
	signal w_l1_wr : std_logic;
	signal w_l1_rd : std_logic;
	signal w_l2_wr : std_logic;
	signal w_l2_rd : std_logic;
	signal w_ram_wr : std_logic;
	signal w_ram_rd : std_logic;
	signal w_ram_addr : std_logic_vector(c_test_addr_width - 1 downto 0);

	-- Clock period definitions
	constant clk_p : time := 10 ns;

begin
 
	-- Instantiate the Unit Under Test (UUT)
	uut: cd_fsm 
		generic map (
			g_data_width => c_test_data_width,
			g_addr_width => c_test_addr_width,
			g_bl => c_test_bl
		)
		port map (
			i_clk => w_clk,
			i_clr => w_clr,
			i_wr => w_wr,
			i_rd => w_rd,
			i_addr => w_addr(c_test_addr_width - 1 downto 0),
			i_data => w_i_data,
			o_w_ack => w_w_ack,
			o_r_ack => w_r_ack,
			o_data => w_o_data,
			o_mem_data => w_mem_data,
			o_l_addr => w_l_addr,
			i_l1_miss => w_l1_miss,
			i_l1_data => w_l1_data,
			o_l1_wr => w_l1_wr,
			o_l1_rd => w_l1_rd,
			i_l2_ready => w_l2_ready,
			i_l2_miss => w_l2_miss,
			i_l2_changed => w_l2_changed,
			i_l2_data => w_l2_data,
			o_l2_wr => w_l2_wr,
			o_l2_rd => w_l2_rd,
			i_ram_ready => w_ram_ready,
			i_ram_changed => w_ram_changed,
			i_ram_data => w_ram_data,
			o_ram_wr => w_ram_wr,
			o_ram_rd => w_ram_rd,
			o_ram_addr => w_ram_addr
		);


	-- Clock
	w_clk <= not w_clk after clk_p / 2;
 

	-- Stimulus process
	stim_proc: process begin	
		-- hold reset state for 100 ns.
		w_clr <= '1';
		wait for 10 * clk_p;
		w_clr <= '0';
		wait for clk_p;
		
		--idle - w_l1 - w_wt
		w_wr <= '1';
		w_i_data <= x"AA";
		w_addr <= x"0111";
		wait for clk_p;
		w_wr <= '0';
		w_l1_miss <= '0';
		w_l1_data <= x"44332211";
		wait for clk_p * 2;
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		w_ram_changed <= '1';
		wait for clk_p;
		w_ram_changed <= '0';
		wait for clk_p * 2;
		w_ram_ready <= '1';
		wait for clk_p;
		w_ram_ready <= '0';
		wait for clk_p;
		
		--idle - w_l1 - w_l2 - w_wt
		w_wr <= '1';
		w_i_data <= x"BB";
		w_addr <= x"0112";
		wait for clk_p;
		w_wr <= '0';
		w_l1_miss <= '1';
		wait for clk_p * 2;
		w_l1_miss <= '0';
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		wait for clk_p;
		w_l2_miss <= '0';
		w_l2_ready <= '1';
		w_l2_data <= x"40302010";
		wait for clk_p;
		w_l2_ready <= '0';
		wait for clk_p;
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		w_ram_changed <= '1';
		wait for clk_p;
		w_ram_changed <= '0';
		wait for clk_p * 2;
		w_ram_ready <= '1';
		wait for clk_p;
		w_ram_ready <= '0';
		wait for clk_p;
		
		--idle - w_l1 - w_l2 - w_ram - w_wt
		w_wr <= '1';
		w_i_data <= x"00";
		w_addr <= x"7777";
		wait for clk_p;
		w_wr <= '0';
		w_l1_miss <= '1';
		wait for clk_p * 2;
		w_l1_miss <= '0';
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		wait for clk_p;
		w_l2_miss <= '1';
		w_l2_ready <= '1';
		wait for clk_p;
		w_l2_miss <= '0';
		w_l2_ready <= '0';
		wait for clk_p;
		w_ram_changed <= '1';
		wait for clk_p;
		w_ram_changed <= '0';
		wait for clk_p * 2;
		w_ram_ready <= '1';
		w_ram_data <= x"11221122";
		wait for clk_p;
		w_ram_ready <= '0';
		wait for clk_p;
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		w_ram_changed <= '1';
		wait for clk_p;
		w_ram_changed <= '0';
		wait for clk_p * 2;
		w_ram_ready <= '1';
		wait for clk_p;
		w_ram_ready <= '0';
		wait for clk_p;
		
		--idle - r_l1
		w_rd <= '1';
		w_addr <= x"0111";
		wait for clk_p;
		w_rd <= '0';
		w_l1_miss <= '0';
		w_l1_data <= x"44332211";
		wait for clk_p;
		
		--idle - r_l1 - r_l2
		w_rd <= '1';
		w_addr <= x"0112";
		wait for clk_p;
		w_rd <= '0';
		w_l1_miss <= '1';
		wait for clk_p;
		w_l1_miss <= '0';
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		wait for clk_p;
		w_l2_ready <= '1';
		w_l2_miss <= '0';
		w_l2_data <= x"01020304";
		wait for clk_p;
		w_l2_ready <= '0';
		wait for clk_p;
		
		--idle - r_l1 - r_l2 - r_ram - r_wt
		w_rd <= '1';
		w_addr <= x"7777";
		wait for clk_p;
		w_rd <= '0';
		w_l1_miss <= '1';
		wait for clk_p;
		w_l1_miss <= '0';
		wait for clk_p;
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		wait for clk_p;
		w_l2_miss <= '1';
		w_l2_ready <= '1';
		wait for clk_p;
		w_l2_miss <= '0';
		w_l2_ready <= '0';
		wait for clk_p;
		w_ram_changed <= '1';
		wait for clk_p;
		w_ram_changed <= '0';
		wait for clk_p * 2;
		w_ram_ready <= '1';
		w_ram_data <= x"11221122";
		wait for clk_p;
		w_ram_ready <= '0';
		wait for clk_p;
		w_l2_changed <= '1';
		wait for clk_p;
		w_l2_changed <= '0';
		wait for clk_p * 2;
		w_l2_ready <= '1';
		wait for clk_p;
		w_l2_ready <= '0';
		wait for clk_p;
		
		wait;
	end process;

end;
