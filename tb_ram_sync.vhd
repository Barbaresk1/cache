library ieee;
use ieee.std_logic_1164.all;

entity tb_ram_sync is
end tb_ram_sync;
 
architecture tb_behavior of tb_ram_sync is
 
	-- Constants for generics
	constant c_test_addr_width : integer := 14;
	constant c_test_data_width : integer := 8;
	constant c_test_bl : integer := 4;
	
	-- Component Declaration for the Unit Under Test (UUT)
	component ram_sync
		generic (
			g_addr_width : integer;
			g_data_width : integer;
			g_bl : integer
		);
		port( 
			i_clk : in std_logic;
			i_clk_op : in std_logic;
			i_clr : in std_logic;
			i_wr : in std_logic;
			i_rd : in std_logic;
			i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
			i_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
			o_data : out std_logic_vector (g_data_width * g_bl - 1 downto 0);
			o_ready : out std_logic;
			o_is_changed : out std_logic
		);
	end component;
  
   --Inputs
   signal w_clk : std_logic := '0';
	signal w_clk_op : std_logic := '0';
   signal w_clr : std_logic := '0';
   signal w_wr : std_logic := '0';
   signal w_rd : std_logic := '0';
   signal w_addr : std_logic_vector(c_test_addr_width - 1 + 2 downto 0) := (others => '0');
   signal w_i_data : std_logic_vector(c_test_data_width * c_test_bl - 1 downto 0) := (others => '0');

 	--Outputs
   signal w_o_data : std_logic_vector(c_test_data_width * c_test_bl - 1 downto 0);
	signal w_ready : std_logic;
	signal w_is_changed : std_logic;

	-- Clock period definitions
	constant clk_p : time := 10 ns;
	constant clk_p_op : time := 110 ns;
 
begin

	-- Instantiate the Unit Under Test (UUT)
	uut: ram_sync
		generic map(
			g_addr_width => c_test_addr_width,
			g_data_width => c_test_data_width,
			g_bl => c_test_bl
		)
		port map(
			i_clk => w_clk,
			i_clk_op => w_clk_op,
			i_clr => w_clr,
			i_wr => w_wr,
			i_rd => w_rd,
			i_addr => w_addr(c_test_addr_width - 1 downto 0),
			i_data => w_i_data,
			o_data => w_o_data,
			o_ready => w_ready,
			o_is_changed => w_is_changed
		);
		

	-- Clock
	w_clk <= not w_clk after clk_p / 2;
	w_clk_op <= not w_clk_op after clk_p_op / 2;
 

 	-- Stimulus process
	stim_proc: process
	begin	
		wait for clk_p_op * 10;	
		w_clr <= '1';
		wait for clk_p_op * 10;
		w_clr <= '0';
		wait for clk_p_op * 10;
		
		-- w_wr(0004) = DDCCBBAA
		w_addr <= x"0005";
		w_wr <= '1';
		w_i_data <= x"DDCCBBAA";
		wait until w_is_changed = '1' and rising_edge(w_clk);
		w_wr <= '0';
		wait until w_ready = '1' and rising_edge(w_clk);
		wait until w_ready = '0' and rising_edge(w_clk);

		--rd(0006)
		w_addr <= x"0006";
		w_rd <= '1';
		wait until w_is_changed = '1' and rising_edge(w_clk); 
		w_rd <= '0';
		wait until w_ready = '1' and rising_edge(w_clk);
		wait until w_ready = '0' and rising_edge(w_clk);

		--rd(3FFF)
		w_addr <= x"3FFF";
		w_rd <= '1';
		wait until w_is_changed = '1' and rising_edge(w_clk); 
		w_rd <= '0';
		wait until w_ready = '1' and rising_edge(w_clk);
		wait until w_ready = '0' and rising_edge(w_clk);
		
		-- w_wr(3FFC) = 00BBAA00
		w_addr <= x"3FFE";
		w_wr <= '1';
		w_i_data <= x"00BBAA00";
		wait until w_is_changed = '1' and rising_edge(w_clk);		
		w_wr <= '0';
		wait until w_ready = '1' and rising_edge(w_clk);
		wait until w_ready = '0' and rising_edge(w_clk);

		--rd(3FFC)
		w_addr <= x"3FFC";
		w_rd <= '1';
		wait until w_is_changed = '1' and rising_edge(w_clk); 		
		w_rd <= '0';
		wait until w_ready = '1' and rising_edge(w_clk);
		wait until w_ready = '0' and rising_edge(w_clk);

      --rd(0006)
		w_addr <= x"0006";
		w_rd <= '1';
		wait until w_is_changed = '1' and rising_edge(w_clk);
		w_rd <= '0';
		wait until w_ready = '1' and rising_edge(w_clk);
		wait until w_ready = '0' and rising_edge(w_clk);
		wait;
	end process;

end;
