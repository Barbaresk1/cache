library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.functions.all;

entity cd_fsm is
	generic(
		g_data_width : integer := 8;
		g_addr_width : integer := 14;
		g_bl : integer := 4
	);
	port( 
		--�����
		i_clk : in std_logic;
		i_clr : in std_logic;
		i_wr : in std_logic;
		i_rd : in std_logic;
		i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
		i_data : in std_logic_vector (g_data_width - 1 downto 0);
		o_w_ack : out std_logic;
		o_r_ack : out std_logic;
		o_data : out std_logic_vector (g_data_width - 1 downto 0);
		--��� ���� � ������
		o_mem_data : out std_logic_vector (g_data_width * g_bl - 1 downto 0);
		--��� ����
		o_l_addr : out std_logic_vector (g_addr_width - 2 - 1 downto 0);		
		--l1
		i_l1_miss : in std_logic;
		i_l1_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
		o_l1_wr : out std_logic;
		o_l1_rd : out std_logic;
		--l2
		i_l2_ready : in std_logic;
		i_l2_miss : in std_logic;
		i_l2_changed : in std_logic;
		i_l2_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
		o_l2_wr : out std_logic;
		o_l2_rd : out std_logic;
		--ram
		i_ram_ready : in std_logic;
		i_ram_changed : in std_logic;
		i_ram_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
		o_ram_wr : out std_logic;
		o_ram_rd : out std_logic;
		o_ram_addr : out std_logic_vector (g_addr_width - 1 downto 0)
	);
end cd_fsm;

architecture cd_fsm_arch of cd_fsm is
	type t_cd_fsm_state is (s_idle, s_w_l1, s_w_l2, s_w_ram, s_w_wt, s_r_l1, s_r_l2, s_r_ram, s_r_wt);
	signal r_state : t_cd_fsm_state;
	signal r_addr : std_logic_vector(g_addr_width - 1 downto 0);
	signal r_mp_buf : std_logic_vector(g_data_width - 1 downto 0);
	signal r_mem_buf : std_logic_vector(g_data_width * g_bl - 1 downto 0);
	signal w_byte_addr : integer range 0 to g_bl - 1;
	signal w_r_ack : std_logic;
	signal r_is_reading : boolean;
	signal r_wait : boolean;
	signal r_wait_ram : boolean;
begin
	o_ram_addr <= r_addr;
	o_l_addr <= r_addr(g_addr_width - 1 downto log2(g_bl));
	w_byte_addr <= conv_integer(unsigned(r_addr(log2(g_bl) - 1 downto 0)));
	o_data <= r_mem_buf(g_data_width * (w_byte_addr + 1) - 1 downto g_data_width * w_byte_addr)
		when r_state = s_idle and r_is_reading else (others => '0');
	o_r_ack <= w_r_ack;
	
	buf_reg: for i in 0 to g_bl - 1 generate
		o_mem_data((i + 1) * g_data_width - 1 downto i * g_data_width) <= r_mem_buf((i + 1) * g_data_width  - 1 downto i * g_data_width) 
			when i /= w_byte_addr or r_is_reading else r_mp_buf;
   end generate buf_reg;

	process (i_clk) begin
		if rising_edge(i_clk) then
			if i_clr = '1' then
				r_state <= s_idle;
				r_addr <= (others => '0');
				r_mp_buf <= (others => '0');
				r_mem_buf <= (others => '0');
				o_l1_rd <= '0';
				o_l1_wr <= '0';
				o_l2_rd <= '0';
				o_l2_wr <= '0';
				o_ram_rd <= '0';
				o_ram_wr <= '0';
				w_r_ack <= '0';
				o_w_ack <= '0';
			else
				--����� ��������� � �������� � ������������ � ����������
				--+ ������ ������ �������� � ������������ � ��������������
				case r_state is
					when s_idle =>
						o_l1_rd <= '0';
						o_l1_wr <= '0';
						o_l2_rd <= '0';
						o_l2_wr <= '0';
						o_ram_rd <= '0';
						o_ram_wr <= '0';
						w_r_ack <= '0';
						o_w_ack <= '0';
						r_wait <= true;
						r_wait_ram <= false;
						if i_wr = '1' then
							r_addr <= i_addr;
							r_mp_buf <= i_data;
							o_l1_rd <= '1';
							r_state <= s_w_l1;
							r_is_reading <= false;
						elsif i_rd = '1' then
							r_addr <= i_addr;
							o_l1_rd <= '1';
							r_state <= s_r_l1;
							r_is_reading <= true;
						else
							r_is_reading <= false;						
						end if;
						
					when s_w_l1 =>
						o_l1_rd <= '0';
						if r_wait then
							r_wait <= false;
						else
							if i_l1_miss = '1' then
								o_l2_rd <= '1';
								r_wait <= true;
								r_state <= s_w_l2;
							else
								r_mem_buf <= i_l1_data;
								o_l1_wr <= '1';
								o_l2_wr <= '1';
								o_ram_wr <= '1';
								r_wait <= true;
								r_state <= s_w_wt;
							end if;
						end if;
						
					when s_w_l2 =>
						if r_wait then
							r_wait <= false;
						else
							if i_l2_changed = '1' then --sync
								o_l2_rd <= '0';
							end if;
							if i_l2_ready = '1' then
								if i_l2_miss = '1' then
									o_ram_rd <= '1';
									r_state <= s_w_ram;
									r_wait <= true;
								else
									r_mem_buf <= i_l2_data;
									o_l1_wr <= '1';
									o_l2_wr <= '1';
									o_ram_wr <= '1';
									r_state <= s_w_wt;
									r_wait <= true;
								end if;
							end if;
						end if;
					when s_w_ram =>
						if r_wait then
							r_wait <= false;
						else
							if i_ram_changed = '1' then --sync
								o_ram_rd <= '0';
							end if;
							if i_ram_ready = '1' then
								r_mem_buf <= i_ram_data;
								r_wait_ram <= true;
							end if;
							if i_ram_ready = '0' and r_wait_ram then
								o_l1_wr <= '1';
								o_l2_wr <= '1';
								o_ram_wr <= '1';
								r_state <= s_w_wt;
								r_wait <= true;
								r_wait_ram <= false;
							end if;
						end if;
						
					when s_w_wt => null;
						if r_wait then
							r_wait <= false;
						else
							o_l1_wr <= '0';
							if i_l2_changed = '1' then --sync
								o_l2_wr <= '0';
							end if;
							if i_ram_changed = '1' then --sync
								o_ram_wr <= '0';
							end if;
							if i_ram_ready = '1' then
								r_wait_ram <= true;
							end if;
							if i_ram_ready = '0' and r_wait_ram then
								o_w_ack <= '1';
								r_state <= s_idle;					
							end if;
						end if;

					when s_r_l1 =>
						o_l1_rd <= '0';
						if r_wait then
							r_wait <= false;
						else
							if i_l1_miss = '1' then
								o_l2_rd <= '1';
								r_wait <= true;
								r_state <= s_r_l2;
							else
								r_mem_buf <= i_l1_data;
								w_r_ack <= '1';
								r_state <= s_idle;
							end if;
						end if;
					when s_r_l2 =>
						if r_wait then
							r_wait <= false;
						else
							if i_l2_changed = '1' then --sync
								o_l2_rd <= '0';
							end if;
							if i_l2_ready = '1' then
								if i_l2_miss = '1' then
									o_ram_rd <= '1';
									r_wait <= true;
									r_state <= s_r_ram;
								else
									r_mem_buf <= i_l2_data;
									o_l1_wr <= '1';
									w_r_ack <= '1';
									r_state <= s_idle;
								end if;
							end if;
						end if;
						
					when s_r_ram =>
						if r_wait then
							r_wait <= false;
						else
							if i_ram_changed = '1' then --sync
								o_ram_rd <= '0';
							end if;
							if i_ram_ready = '1' then
								r_wait_ram <= true;
								r_mem_buf <= i_ram_data;
							end if;
							if r_wait_ram and i_ram_ready = '0' then
								o_l1_wr <= '1';
								o_l2_wr <= '1';
								r_wait <= true;
								r_state <= s_r_wt;								
							end if;
						end if;
						
					when s_r_wt =>
						if r_wait then
							r_wait <= false;
						else
							o_l1_wr <= '0';
							if i_l2_changed = '1' then --sync
								o_l2_wr <= '0';
							end if;
							if i_l2_ready = '1' then
								w_r_ack <= '1';
								r_state <= s_idle;
							end if;
						end if;
						
					when others => null;
				end case;
			end if;
		end if;
	end process;
end cd_fsm_arch;

