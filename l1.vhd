library ieee;
use ieee.std_logic_1164.all;

entity l1 is 
	generic(
		g_addr_width : integer := 12;
		g_data_width : integer := 32;
		g_memory_depth : integer := 32
	);
	port( 
		i_clk : in std_logic;
		i_clr : in std_logic;
		i_wr : in std_logic;
		i_rd : in std_logic;
		i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
		i_data : in std_logic_vector (g_data_width - 1 downto 0);
		o_miss : out std_logic;
		o_data : out std_logic_vector (g_data_width - 1 downto 0)
	);
end l1;

architecture l1_arch of l1 is

   component lfuda
		generic (
			g_addr_width : integer;
			g_data_width : integer;
			g_memory_depth : integer
		);
		port( 
			i_clk : in std_logic;
			i_clr : in std_logic;
			i_wr : in std_logic;
			i_rd : in std_logic;
			i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_miss : out std_logic;
			o_data : out std_logic_vector (g_data_width - 1 downto 0)
		);
	end component;
	
	signal w_bit : std_logic;
	signal w_data_0 : std_logic_vector (g_data_width - 1 downto 0);
	signal w_data_1 : std_logic_vector (g_data_width - 1 downto 0);
	signal w_wr_0 : std_logic;
	signal w_wr_1 : std_logic;
	signal w_miss_0 : std_logic;
	signal w_miss_1 : std_logic;
	
begin

	--0 � ������ ����
	l1c: lfuda
		generic map(
			g_addr_width => g_addr_width - 1,
			g_data_width => g_data_width,
			g_memory_depth => g_memory_depth / 2
		)
		port map(
			i_clr => i_clr,
			i_clk => i_clk,
			i_wr => w_wr_0,
			i_rd => i_rd,
			i_addr => i_addr(g_addr_width - 2 downto 0),
			i_data => i_data,
			o_miss => w_miss_0,
			o_data => w_data_0
		);
	
	--1 � ������ ����
	l1d: lfuda
		generic map(
			g_addr_width => g_addr_width - 1,
			g_data_width => g_data_width,
			g_memory_depth => g_memory_depth / 2
		)
		port map(
			i_clr => i_clr,
			i_clk => i_clk,
			i_wr => w_wr_1,
			i_rd => i_rd,
			i_addr => i_addr(g_addr_width - 2 downto 0),
			i_data => i_data,
			o_miss => w_miss_1,
			o_data => w_data_1
		);

	w_bit <= i_addr(g_addr_width - 1);
	w_wr_0 <= i_wr and not w_bit;
	w_wr_1 <= i_wr and w_bit;
	o_data <= w_data_0 when w_bit = '0' else w_data_1;
	o_miss <= w_miss_0 when w_bit = '0' else w_miss_1;
	
end l1_arch;

