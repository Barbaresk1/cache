library ieee;
use ieee.std_logic_1164.all;
 
entity tb_sync is
end tb_sync;
 
architecture tb_behavior of tb_sync is 
 
	-- Constants for generics
	constant c_test_data_width : integer := 8;

	-- Component Declaration for the Unit Under Test (UUT)
	component sync
		generic (
			g_data_width : integer
		);
		port( 
			i_clr : in std_logic;
			i_clk_1 : in std_logic;
			i_clk_2 : in std_logic;
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_data : out std_logic_vector (g_data_width - 1 downto 0);
			o_correct : out std_logic;
			o_is_changed : out std_logic
		);
	end component;

   --Inputs
   signal w_clr : std_logic := '0';
   signal w_clk_1 : std_logic := '0';
   signal w_clk_2 : std_logic := '0';
   signal w_i_data : std_logic_vector(c_test_data_width - 1 downto 0) := (others => '0');

 	--Outputs
   signal w_o_data : std_logic_vector(c_test_data_width - 1 downto 0);
   signal w_correct : std_logic;
	signal w_is_changed : std_logic;
 
	-- Clock period definitions
	--������ ������� � ����� ������������ ��� �������� ��������
	constant clk_p_1 : time := 10 ns;
	constant clk_p_2 : time := 20 ns;
  
begin

	-- Instantiate the Unit Under Test (UUT)
	uut: sync
		generic map(
			g_data_width => c_test_data_width
		)
		port map(
			i_clr => w_clr,
			i_clk_1 => w_clk_1,
			i_clk_2 => w_clk_2,
			i_data => w_i_data,
			o_data => w_o_data,
			o_correct => w_correct,
			o_is_changed => w_is_changed
		);
		
	-- Clock
	w_clk_1 <= not w_clk_1 after clk_p_1 / 2;
	w_clk_2 <= not w_clk_2 after clk_p_2 / 2;
		

	-- Stimulus process
	stim_proc: process
	begin	
		-- hold reset state for 100 ns.
		w_clr <= '1';
		wait for 100 ns;
		w_clr <= '0';
		w_i_data <= x"FA";
		wait for 100 ns;
		w_i_data <= x"AA";
		wait for 100 ns;
		w_i_data <= x"00";
		wait for 100 ns;
		w_i_data <= x"0A";
		wait for 100 ns;
      wait;
   end process;

END;
