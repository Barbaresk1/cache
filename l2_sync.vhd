library ieee;
use ieee.std_logic_1164.all;

entity l2_sync is
	generic(
		g_addr_width : integer := 12;
		g_data_width : integer := 32;
		g_memory_depth : integer := 64
	);
	port( 
		i_clk : in std_logic;
		i_clk_2 : in std_logic;
		i_clr : in std_logic;
		i_wr : in std_logic;
		i_rd : in std_logic;
		i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
		i_data : in std_logic_vector (g_data_width - 1 downto 0);
		o_miss : out std_logic;
		o_data : out std_logic_vector (g_data_width - 1 downto 0);
		o_ready : out std_logic;
		o_is_changed : out std_logic
	);
end l2_sync;

architecture l2_sync_arch of l2_sync is

	component sync
		generic (
			g_data_width : integer
		);
		port( 
			i_clr : in std_logic;
			i_clk_1 : in std_logic;
			i_clk_2 : in std_logic;
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_data : out std_logic_vector (g_data_width - 1 downto 0);
			o_correct : out std_logic;
			o_is_changed : out std_logic
		);
	end component;
	
	component lru
		generic (
			g_addr_width : integer;
			g_data_width : integer;
			g_memory_depth : integer
		);
		port( 
			i_clk : in std_logic;
			i_clr : in std_logic;
			i_wr : in std_logic;
			i_rd : in std_logic;
			i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_miss : out std_logic;
			o_data : out std_logic_vector (g_data_width - 1 downto 0);
			o_ready : out std_logic
		);
	end component;
	
	constant c_width_i : integer := 1 + 1 + g_addr_width + g_data_width;
	constant c_width_o : integer := 1 + 1 + g_data_width;
	
	signal w_in : std_logic_vector(c_width_i - 1 downto 0);
	signal w_out : std_logic_vector(c_width_o - 1 downto 0);
	signal w_data_i : std_logic_vector(c_width_i - 1 downto 0);
	signal w_data_o : std_logic_vector(c_width_o - 1 downto 0);
begin
	lru_inst: lru
		generic map(
			g_addr_width => g_addr_width,
			g_data_width => g_data_width,
			g_memory_depth => g_memory_depth
		)
		port map(
			i_clr => i_clr,
			i_clk => i_clk_2,
			i_wr => w_in(c_width_i - 1),
			i_rd => w_in(c_width_i - 2),
			i_addr => w_in(g_addr_width + g_data_width - 1 downto g_data_width),
			i_data => w_in(g_data_width - 1 downto 0),
			o_miss => w_out(c_width_o - 2),
			o_data => w_out(g_data_width - 1 downto 0),
			o_ready => w_out(c_width_o - 1)
		);
		
	sync_i: sync
		generic map(
			g_data_width => c_width_i
		)
		port map(
			i_clr => i_clr,
			i_clk_1 => i_clk,
			i_clk_2 => i_clk_2,
			i_data => w_data_i,
			o_data => w_in,
			o_correct => open,
			o_is_changed => o_is_changed
		);
		
	sync_o: sync
		generic map(
			g_data_width => c_width_o
		)
		port map(
			i_clr => i_clr,
			i_clk_1 => i_clk_2,
			i_clk_2 => i_clk,
			i_data => w_out,
			o_data => w_data_o,
			o_correct => open,
			o_is_changed => open
		);
	
	w_data_i <= i_wr & i_rd & i_addr & i_data;
	o_data <= w_data_o(g_data_width - 1 downto 0);
	o_ready <= w_data_o(c_width_o - 1);
	o_miss <= w_data_o(c_width_o - 2);


end l2_sync_arch;

