library ieee;
use ieee.std_logic_1164.all;

entity lfuda is 
	generic(
		g_addr_width : integer;
		g_data_width : integer;
		g_memory_depth : integer
	);
	port( 
		i_clk : in std_logic;
		i_clr : in std_logic;
		i_wr : in std_logic;
		i_rd : in std_logic;
		i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
		i_data : in std_logic_vector (g_data_width - 1 downto 0);
		o_miss : out std_logic;
		o_data : out std_logic_vector (g_data_width - 1 downto 0)
	);
end lfuda;

architecture lfuda_arch of lfuda is
	type t_lfuda_cell is record
		tag : std_logic_vector(g_addr_width - 1 downto 0);
		mem : std_logic_vector(g_data_width - 1 downto 0);
		age : integer;
	end record t_lfuda_cell;
	type t_lfuda_memory is array (g_memory_depth - 1 downto 0) of t_lfuda_cell; 
	signal r_mem : t_lfuda_memory;
	signal r_count : integer;
	signal w_cur_tag_position : integer;
begin
	
	process (i_clk) 
		variable find : integer;
		variable age : integer;
	begin
		if rising_edge(i_clk) then
			if i_clr = '1' then
				r_mem <= (others => ((others => '0'), (others => '0'), 0));
				r_count <= 0;
				o_miss <= '0';
				o_data <= (others => '0');
			elsif i_wr = '1' or i_rd = '1' then
				find := w_cur_tag_position;
				--��������� ��������
				if i_wr = '1' then
					--������� ������� 
					if find >= r_count then
						if r_count < g_memory_depth then
							r_count <= 1 + r_count;
						end if;
						find := g_memory_depth;
					end if;
					
					--��������, ����� ��� ���
					if find = g_memory_depth then --���� �� �����
						age := r_mem(0).age;
						find := 0;
						--���� �����, ���� ������
						for i in 0 to g_memory_depth - 1 loop
							if r_mem(i).age < age then
								age := r_mem(i).age;
								find := i;
							end if;
						end loop;
						r_mem(find).age <= r_mem(find).age + 1;
						r_mem(find).tag <= i_addr;
						r_mem(find).mem <= i_data;
						o_miss <= '1';
					else
						r_mem(find).age <= r_mem(find).age + 1; --�������� �����
						r_mem(find).mem <= i_data;
						o_miss <= '0';
					end if;
					--����� ��� ������������ (�� ��������)
--					if r_mem(find).age = 0 then
--						for i in 0 to g_memory_depth - 1 loop
--							r_mem(i).age <= r_mem(i).age / 2;
--						end loop;
--					end if;
				else
					if find >= r_count then --���� �� ����� �� ������ - ������
						o_miss <= '1';
					else --���� �����
						r_mem(find).age <= r_mem(find).age + 1;
						o_data <= r_mem(find).mem;
						o_miss <= '0';
						--����� ��� ������������
						if r_mem(find).age = 0  then
							for i in 0 to g_memory_depth  - 1 loop
								r_mem(i).age <= r_mem(i).age / 2;
							end loop;
						end if;
					end if;
				end if;
			else
				o_miss <= '0';
				o_data <= (others => '0');
			end if;
		end if;
	end process;
	
	--���������� ������� ��� �������
	process (i_addr, r_mem)
		variable i:integer;
	begin
		w_cur_tag_position <= g_memory_depth;
		for i in g_memory_depth - 1 downto 0 loop
			if i_addr = r_mem(i).tag then
				w_cur_tag_position <= i;
			end if;
		end loop;
	end process;
end lfuda_arch;

