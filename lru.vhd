library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity lru is
	generic(
		g_addr_width : integer;
		g_data_width : integer;
		g_memory_depth : integer
	);
	port( 
		i_clk : in std_logic;
		i_clr : in std_logic;
		i_wr : in std_logic;
		i_rd : in std_logic;
		i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
		i_data : in std_logic_vector (g_data_width - 1 downto 0);
		o_miss : out std_logic;
		o_data : out std_logic_vector (g_data_width - 1 downto 0);
		o_ready : out std_logic
	);
end lru;
	
architecture lru_arch of lru is
	type t_lru_cell is record
		tag : std_logic_vector(g_addr_width - 1 downto 0);
		mem : std_logic_vector(g_data_width - 1 downto 0);
	end record t_lru_cell;
	type t_lru_memory is array (g_memory_depth - 1 downto 0) of t_lru_cell; 
	signal r_mem : t_lru_memory;
	signal r_count : integer;
	signal w_cur_tag_position : integer;
begin

	process (i_clk) 
		variable i : integer;
		variable find : integer := g_memory_depth;
		variable result : std_logic_vector(g_data_width - 1 downto 0);
	begin
		if rising_edge(i_clk) then
			if i_clr = '1' then
				r_mem <= (others => ((others => '0'), (others => '0')));
				r_count <= 0;
			elsif i_wr = '1' or i_rd = '1' then
				find := w_cur_tag_position;
				--��������� ��������
				if i_wr = '1' then
					--������� ������� 
					if find > r_count then
						r_count <= 1 + r_count;
					end if;
					--�������� ����������
					for i in find downto 1 loop
						if i /= g_memory_depth then
							r_mem(i) <= r_mem(i - 1);
						end if;
					end loop;
					r_mem(0).mem <= i_data;
					r_mem(0).tag <= i_addr;
				else
					if find >= r_count then
						o_miss <= '1';
					else
						--�������� ���������� 
						result := r_mem(find).mem;
						for i in find downto 1 loop
							if i /= g_memory_depth then
								r_mem(i) <= r_mem(i - 1);
							end if;
						end loop;
						r_mem(0).mem <= result;
						r_mem(0).tag <= i_addr;
						o_data <= result;
						o_miss <= '0';
					end if;
				end if;
				o_ready <= '1';
			else
				o_miss <= '0';
				o_data <= (others => '0');
				o_ready <= '0';
			end if;
		end if;
	end process;
	
	--���������� ������� ��� �������
	process (i_addr, r_mem)
		variable i:integer;
	begin
		w_cur_tag_position <= g_memory_depth;
		for i in g_memory_depth - 1 downto 0 loop
			if i_addr = r_mem(i).tag then
				w_cur_tag_position <= i;
			end if;
		end loop;
	end process;
		
end lru_arch;

