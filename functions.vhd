library ieee;
use ieee.std_logic_1164.all;

package functions is
	function log2 (x : integer) return integer;
end functions;

package body functions is

	function log2 (x : integer) return integer is
		variable i : integer;
	begin
		i := 0;  
		while (2 ** i < x) and i < 31 loop
			i := i + 1;
		end loop;
		return i;
	end log2;
end functions;
