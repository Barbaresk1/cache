library ieee;
use ieee.std_logic_1164.all;

entity cd_ram is
	generic(
		g_addr_width : integer;
		g_data_width : integer;
		g_bl : integer
	);
	port( 
		i_clk : in std_logic;
		i_clr : in std_logic;
		i_wr : in std_logic;
		i_rd : in std_logic;
		i_addr : in std_logic_vector (g_addr_width - 1 downto 0);
		i_data : in std_logic_vector (g_data_width * g_bl - 1 downto 0);
		o_data : out std_logic_vector (g_data_width * g_bl - 1 downto 0);
		o_ready : out std_logic
	);
end cd_ram;

architecture cd_ram_arch of cd_ram is
	component ram
		generic(
			g_addr_width : integer;
			g_data_width : integer;
			g_bl : integer
		);
		port( 
			i_clk : in std_logic;
			i_clr : in std_logic;
			i_cas : in std_logic;
			i_ras : in std_logic;
			i_wr : in std_logic;
			i_cs : in std_logic;
			i_addr : in std_logic_vector (g_addr_width / 2 - 1 downto 0);
			i_data : in std_logic_vector (g_data_width - 1 downto 0);
			o_data : out std_logic_vector (g_data_width - 1 downto 0)
		);
	end component;

	signal w_cas : std_logic;
	signal w_ras : std_logic;
	signal w_cs : std_logic;
	signal w_wr : std_logic;
	signal w_addr : std_logic_vector (g_addr_width / 2 - 1 downto 0);
	signal w_data_i : std_logic_vector (g_data_width - 1 downto 0);
	signal w_data_o : std_logic_vector (g_data_width - 1 downto 0);
	signal r_data : std_logic_vector (g_data_width * g_bl - 1 downto 0); --������ ������ (� ������ ��� �� ������)
	signal r_addr : std_logic_vector (g_addr_width - 1 downto 0);
	type t_cd_ram_state is (s_idle, s_write, s_read);
	signal r_state : t_cd_ram_state;
	signal r_count : integer;
begin
	ram_inst : ram 
		generic map(
			g_addr_width => g_addr_width,
			g_data_width => g_data_width,
			g_bl => g_bl
		)
		port map(
			i_clk => i_clk,
			i_clr => i_clr,
			i_cas => w_cas,
			i_ras => w_ras,
			i_wr => w_wr,
			i_cs => w_cs,
			i_addr => w_addr,
			i_data => w_data_i,
			o_data => w_data_o
		);
		
	w_ras <= '1' when r_state = s_idle else '0';
	w_addr <= r_addr(g_addr_width - 1 downto g_addr_width / 2) when r_state = s_idle
		else r_addr(g_addr_width / 2 - 1 downto 0);
	r_addr <= i_addr when r_state = s_idle else r_addr;
	
	process (i_clk) begin
		if rising_edge(i_clk) then
			if i_clr = '1' then
				w_cas <= '0';
				w_cs <= '0';
				w_wr <= '0';
				r_state <= s_idle;
				r_count <= g_bl;
				r_data <= (others => '0');
				o_data <= (others => '0');
				o_ready <= '0';
			else
				case r_state is
					when s_idle =>
						o_ready <= '0';
						w_cas <= '0';
						w_cs <= '0';
						w_wr <= '0';
						if i_wr = '1' or i_rd = '1' then
							w_cas <= '1';
							if i_wr = '1' then
								r_state <= s_write;
								r_count <= 0;
							else
								r_state <= s_read;
								r_count <= -2;
							end if;
						end if;
						r_data <= i_data;
						o_data <= (others => '0');
					when s_write =>
						w_cas <= '0';
						w_wr <= '1';
						w_data_i <= r_data(g_data_width * (r_count + 1) - 1 downto g_data_width * r_count);			
						if r_count + 1 = g_bl then
							r_state <= s_idle;
							o_ready <= '1';
							w_wr <= '0';
						end if;
						r_count <= r_count + 1;
					when s_read =>
						w_cas <= '0';
						if r_count >= 0 then
							w_cs <= '0';
						else
							w_cs <= '1';
						end if;
						if r_count >= 0 then
							r_data(g_data_width * (r_count + 1) - 1 downto g_data_width * r_count) <= w_data_o;
						end if;
						if r_count + 1 = g_bl then
							r_state <= s_idle;
							o_data <= w_data_o & r_data(g_data_width * r_count - 1 downto 0);
							o_ready <= '1';
							w_cs <= '0';
						end if;
						r_count <= r_count + 1;
				end case;						
			end if;
		end if;
	end process;

end cd_ram_arch;

