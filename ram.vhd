library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.functions.all;

entity ram is
	generic(
		g_addr_width : integer;
		g_data_width : integer;
		g_bl : integer
	);
	port( 
		i_clk : in std_logic;
		i_clr : in std_logic;
		i_cas : in std_logic;
		i_ras : in std_logic;
		i_wr : in std_logic;
		i_cs : in std_logic;
		i_addr : in std_logic_vector (g_addr_width / 2 - 1 downto 0);
		i_data : in std_logic_vector (g_data_width - 1 downto 0);
		o_data : out std_logic_vector (g_data_width - 1 downto 0)
	);
end ram;

architecture ram_arch of ram is
	constant c_memory_depth : integer := 2 ** g_addr_width;
	type t_ram_memory is array (c_memory_depth - 1 downto 0) of std_logic_vector(g_data_width - 1 downto 0);
	signal r_mem : t_ram_memory;
	type t_ram_state is (s_idle, s_read, s_write);
	signal r_state : t_ram_state;
	signal r_address : std_logic_vector(g_addr_width - 1 downto 0); 
	signal w_addr : integer;
	signal r_counter : integer;
begin

	w_addr <= conv_integer(unsigned(r_address));

	process (i_clk) begin
		if rising_edge(i_clk) then
			if i_clr = '1' then
				r_mem <= (others => (others => '0'));
				r_state <= s_idle;
				r_address <= (others => '0');
				o_data <= (others => '0');
			else
				case r_state is
					when s_idle =>
						o_data <= (others => '0');
						if i_wr = '1' then
							r_mem(w_addr) <= i_data;
							r_counter <= 1;
							r_state <= s_write;
						elsif i_cs = '1' then
							o_data <= r_mem(w_addr);
							r_counter <= 1;
							r_state <= s_read;
						elsif i_ras = '1' then
							r_address(g_addr_width - 1 downto g_addr_width / 2) <= i_addr;
						elsif i_cas = '1' then
							r_address(g_addr_width / 2 - 1 downto log2(g_bl)) <= i_addr(g_addr_width / 2 - 1 downto log2(g_bl));
							r_address(log2(g_bl) - 1 downto 0) <= (others => '0');
						end if;
					when s_read =>
						o_data <= r_mem(w_addr + r_counter);
						if r_counter + 1 = g_bl then
							r_state <= s_idle;
						end if;
						r_counter <= r_counter + 1;
					when s_write =>
						r_mem(w_addr + r_counter) <= i_data;
						if r_counter + 1 = g_bl then
							r_state <= s_idle;
						end if;
						r_counter <= r_counter + 1;
				end case;
			end if;
		end if;			
	end process;
end ram_arch;

